﻿FileEncoding, UTF-8
clipboard_bak = %clipboard%
WinActivate, ahk_exe chrome.exe
Send, ^a^c
ClipWait,
MsgBox, %clipboard%
RegExMatch(clipboard, "rtmp://.*", url)
if url = 
    Goto, ChromeError
RegExMatch(clipboard, "[0-9a-f]{40}", key)

IfWinNotExist, OBS
    Goto, ObsError
IfWinExist, 設定
    Goto, ObsError
WinActivate, OBS
WinWaitActive, OBS
Send, !fs
WinWaitActive, 設定
clipboard = %url%
ClipWait,
Send, {Down}{Tab}{Up}{Down}{Tab}{Tab}{Tab}{Tab}{Tab}^v{Tab}
clipboard = %key%
ClipWait,
Send, ^v
Goto, Exit

ChromeError:
MsgBox, 
(
ChromeでOPENRECのライブ配信のページを開き、配信URLを発行した後の画面で実行してください。
実行できない場合、Chromeの画面を一度クリックしてから再度実行してください。
)
Goto, Exit

ObsError:
MsgBox, 
(
OBSを起動してから実行してください。
設定ウィンドウは開かずに実行してください。
OBS Classicには対応していません。
)
Goto, Exit

Exit: 
clipboard = %clipboard_bak%
