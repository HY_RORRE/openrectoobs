# README

OPENREC.tvの配信URLおよびストリームキーを自動でOBS Studioに設定するツールです。

[日本語での詳しい情報はこちら](https://hyrorre.com/post/openrec2obs/)

[ダウンロード](https://bitbucket.org/HY_RORRE/openrectoobs/downloads/)